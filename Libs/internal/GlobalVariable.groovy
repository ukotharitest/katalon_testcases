package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object DIDFrom
     
    /**
     * <p></p>
     */
    public static Object DIDTo
     
    /**
     * <p></p>
     */
    public static Object ServicePackage
     
    /**
     * <p></p>
     */
    public static Object DIDNumber
     
    /**
     * <p></p>
     */
    public static Object UserName
     
    /**
     * <p></p>
     */
    public static Object URL
     
    /**
     * <p></p>
     */
    public static Object Password
     
    /**
     * <p></p>
     */
    public static Object TenantName
     
    /**
     * <p></p>
     */
    public static Object ExtensionName
     
    /**
     * <p></p>
     */
    public static Object AudioFile1
     
    /**
     * <p></p>
     */
    public static Object AudioFile2
     
    /**
     * <p></p>
     */
    public static Object AudioPhrase
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['DIDFrom' : null, 'DIDTo' : null, 'ServicePackage' : null, 'DIDNumber' : null, 'UserName' : 'ntadmin1', 'URL' : 'http://www2.carrierip.net/auth/auth/login', 'Password' : 'n3tcarrier@ABC', 'TenantName' : null, 'ExtensionName' : null, 'AudioFile1' : null, 'AudioFile2' : null, 'AudioPhrase' : null])
        
        String profileName = RunConfiguration.getExecutionProfile()
        
        def selectedVariables = allVariables[profileName]
        DIDFrom = selectedVariables['DIDFrom']
        DIDTo = selectedVariables['DIDTo']
        ServicePackage = selectedVariables['ServicePackage']
        DIDNumber = selectedVariables['DIDNumber']
        UserName = selectedVariables['UserName']
        URL = selectedVariables['URL']
        Password = selectedVariables['Password']
        TenantName = selectedVariables['TenantName']
        ExtensionName = selectedVariables['ExtensionName']
        AudioFile1 = selectedVariables['AudioFile1']
        AudioFile2 = selectedVariables['AudioFile2']
        AudioPhrase = selectedVariables['AudioPhrase']
        
    }
}
