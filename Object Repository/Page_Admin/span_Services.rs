<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Services</name>
   <tag></tag>
   <elementGuidId>85bea2b8-e5df-4975-b991-42addb1b783a</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Services</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;site-menu&quot;)/li[@class=&quot;menu-title&quot;]/ul[@class=&quot;main-menu&quot;]/li[@class=&quot;sub-item&quot;]/a[1]/span[1]</value>
   </webElementProperties>
</WebElementEntity>
