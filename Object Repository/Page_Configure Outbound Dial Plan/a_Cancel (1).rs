<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Cancel (1)</name>
   <tag></tag>
   <elementGuidId>e8e9e65c-16d6-4cb1-8d6e-d7a4f731f4ed</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-danger  btn-round-right</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://www2.carrierip.net/tenantmaster/tenantmaster/index?TenantMasterSearch%5Btm_name%5D=hAdz6&amp;TenantMasterSearch%5Btm_email%5D=&amp;TenantMasterSearch%5Btm_phone_number%5D=&amp;TenantMasterSearch%5Btm_status%5D=&amp;TenantMasterSearch%5Bsp_id%5D=&amp;_pjax=%23pjax-tenant-master-index</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cancel</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;tm-tenantmaster-dialplan-form&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;form-group col-sm-offset-5 col-md-offset-5 col-xs-offset-2&quot;]/a[@class=&quot;btn btn-danger  btn-round-right&quot;]</value>
   </webElementProperties>
</WebElementEntity>
