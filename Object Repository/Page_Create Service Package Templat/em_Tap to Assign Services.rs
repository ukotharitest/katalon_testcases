<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>em_Tap to Assign Services</name>
   <tag></tag>
   <elementGuidId>04f0ce6c-7e43-4dce-9537-8327546ba626</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>em</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tap to Assign Services</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;w0&quot;)/div[@class=&quot;card-accordions&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-header&quot;]/a[@class=&quot;card-title&quot;]/h6[@class=&quot;mb-0&quot;]/b[1]/em[@class=&quot;text-primary&quot;]</value>
   </webElementProperties>
</WebElementEntity>
