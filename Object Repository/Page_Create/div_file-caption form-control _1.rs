<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_file-caption form-control _1</name>
   <tag></tag>
   <elementGuidId>023a9c62-d117-4640-93be-8fc582d9661a</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>file-caption form-control  kv-fileinput-caption</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>500</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;uplodefile&quot;)/div[@class=&quot;file-input file-input-new&quot;]/div[@class=&quot;input-group input-group-sm&quot;]/div[@class=&quot;file-caption form-control  kv-fileinput-caption&quot;]</value>
   </webElementProperties>
</WebElementEntity>
