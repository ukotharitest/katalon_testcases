<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img</name>
   <tag></tag>
   <elementGuidId>70af62ac-65a4-46c2-b5c7-df071a32f031</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/themes/assets/global/image/custom/identity_icon/admin.png</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Profile image</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;wrapper&quot;]/section[@id=&quot;header&quot;]/header[@id=&quot;header&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-9 col-xl-10 col-xs-12 pb-1  header-right&quot;]/div[@class=&quot;col-sm-8 col-xl-8 col-xs-12 serch_prt&quot;]/div[@class=&quot;header-inner-right&quot;]/div[@class=&quot;user-dropdown pull-right common-right&quot;]/div[@class=&quot;btn-group&quot;]/a[@class=&quot;user-header dropdown-toggle&quot;]/img[1]</value>
   </webElementProperties>
</WebElementEntity>
