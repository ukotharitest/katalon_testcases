<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Sign into your account</name>
   <tag></tag>
   <elementGuidId>2a560cc7-128f-4298-abc1-5563909ef8ce</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login_v2</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
                
                    
                
                Sign into your account
                

                                        
                    
                                        
                    
                                        
                    
                    
                
                Please fill out the following fields to login:

                

                

User Name or Email Id cannot be blank.

                



                
                    
                        Forgot password?
                    
                

                
                    
                        Login                    
                

                
            
        
    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;login_v2&quot;]</value>
   </webElementProperties>
</WebElementEntity>
