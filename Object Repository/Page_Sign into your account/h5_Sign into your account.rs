<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Sign into your account</name>
   <tag></tag>
   <elementGuidId>58d3694e-6e38-42ba-a2d7-026e8f7749dc</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign into your account</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;login_v2&quot;]/div[@class=&quot;login_v2_main&quot;]/div[@class=&quot;login_v2_contain&quot;]/div[@class=&quot;login_v2_form text-xs-center&quot;]/h5[1]</value>
   </webElementProperties>
</WebElementEntity>
