<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>b_4</name>
   <tag></tag>
   <elementGuidId>fb3a2ef6-ceb0-470f-a2fe-4f654fa1f970</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;globalConfForm&quot;]/div[2]/div/div/div[1]/div/div/span/span[1]/span/span[2]/b</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>b</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>presentation</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;globalConfForm&quot;]/div[2]/div/div/div[1]/div/div/span/span[1]/span/span[2]/b</value>
   </webElementProperties>
</WebElementEntity>
