<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h6_Search</name>
   <tag></tag>
   <elementGuidId>0b055ce4-4210-4bc5-b2fe-1ab508b41397</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mb-0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Search                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;extension_master_search&quot;)/div[@class=&quot;card-accordions&quot;]/div[@id=&quot;accordion&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-header card-custom&quot;]/a[@class=&quot;card-title&quot;]/h6[@class=&quot;mb-0&quot;]</value>
   </webElementProperties>
</WebElementEntity>
