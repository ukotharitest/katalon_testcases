import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.testng.Assert as Assert
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.click(findTestObject('Page_Admin/a_Tenant'))

not_run: WebUI.click(findTestObject('Page_Tenant/h6_Search'))

not_run: WebUI.setText(findTestObject('Page_Tenant/input_TenantMasterSearchtm_nam'), GlobalVariable.TenantName)

not_run: WebUI.click(findTestObject('Page_Tenant/button_Search'))

WebUI.delay(3)

not_run: WebUI.click(findTestObject('Page_Tenant/a_btn btn-warning btn-sm'))

WebUI.click(findTestObject('Page_Tenant/i_fa fa-sign-in'))

WebUI.delay(3)

WebUI.switchToWindowTitle('Tenant Dashboard')

WebUI.delay(3)

not_run: WebUI.switchToWindowTitle('DID')

WebUI.click(findTestObject('Page_DID/a_Manage DID'))

WebUI.click(findTestObject('Page_DID/h6_Search'))

WebUI.setText(findTestObject('Page_DID/input_DidMasterSearchdid_numbe'), GlobalVariable.DIDNumber)

WebUI.click(findTestObject('Page_DID/button_Search'))

String getDIDName = WebUI.getText(findTestObject('Page_DID/td_2674991212'))

if (getDIDName.equals(GlobalVariable.DIDNumber)) {
    Assert.assertTrue(true)
} else {
    Assert.assertTrue(false)
}

WebUI.delay(3)

WebUI.click(findTestObject('Page_Tenant/b_Mr. Admin'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Tenant/a_Logout'))

WebUI.delay(3)

WebUI.closeWindowIndex(1)

WebUI.delay(2)

WebUI.switchToWindowTitle('Tenant')

WebUI.delay(3)

static String randomString(String chars, int length) {
    Random rand = new Random()

    StringBuilder sb = new StringBuilder()

    for (int i = 0; i < length; i++) {
        sb.append(chars.charAt(rand.nextInt(chars.length())))
    }
    
    return sb.toString()
}

