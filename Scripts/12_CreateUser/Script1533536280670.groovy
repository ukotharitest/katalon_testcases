import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.testng.Assert as Assert
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Page_Admin/a_Tenant'))

WebUI.click(findTestObject('Page_Tenant/h6_Search'))

WebUI.delay(3)

WebUI.setText(findTestObject('Page_Tenant/input_TenantMasterSearchtm_nam'), GlobalVariable.TenantName)

WebUI.click(findTestObject('Page_Tenant/button_Search'))

WebUI.click(findTestObject('Page_Tenant/i_fa fa-sign-in'))

WebUI.delay(3)

WebUI.switchToWindowTitle('Tenant Dashboard')

WebUI.click(findTestObject('Object Repository/Page_Tenant Dashboard/i_icon_plus'))

WebUI.click(findTestObject('Object Repository/Page_Tenant Dashboard/a_Users'))

WebUI.click(findTestObject('Object Repository/Page_User/a_Add New'))

WebUI.delay(2)

String chars = 'abcdefghijklmnopqrstuvwxyz0123456789'

String userName = ''

userName = randomString('AUTOMATION' + chars, 5)

GlobalVariable.ExtensionName = userName

WebUI.setText(findTestObject('Object Repository/Page_Create User/input_ExtensionMasterem_name'), 'ExtFName' + userName)

WebUI.setText(findTestObject('Object Repository/Page_Create User/input_ExtensionMasterem_last_n'), 'ExtLName' + userName)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Create User/input_ExtensionMasterem_passwo'), '7Y8p/8u5B6B8OO64kUAV4fvvn7o3hI26')

Random rand = new Random()

int ExtensionNumberInt = rand.nextInt(99999999)

String ExtensionNumber = ExtensionNumberInt.toString()

WebUI.setText(findTestObject('Object Repository/Page_Create User/input_ExtensionMasterem_number'), ExtensionNumber)

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/Page_Create User/button_Save'), 5)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_Create User/button_Save'))

WebUI.click(findTestObject('Object Repository/Page_User/h6_Search'))

WebUI.setText(findTestObject('Object Repository/Page_User/input_ExtensionMasterSearchem_'), userName)

WebUI.click(findTestObject('Object Repository/Page_User/button_Search'))

//String FirstLastName = 'ExtFName' + userName + '' + 'ExtLName' + userName
//WebUI.click(findTestObject('Object Repository/Page_User/td_TestUser LastName'))
String firstlastname = WebUI.getText(findTestObject('Object Repository/Page_User/td_TestUser LastName'))

if (firstlastname.contains(userName)) {
    Assert.assertTrue(true)
} else {
    Assert.assertTrue(false)
}

WebUI.delay(3)

WebUI.click(findTestObject('Page_Tenant/b_Mr. Admin'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Tenant/a_Logout'))

WebUI.delay(3)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Tenant')

WebUI.delay(3)

static String randomString(String chars, int length) {
    Random rand = new Random()

    StringBuilder sb = new StringBuilder()

    for (int i = 0; i < length; i++) {
        sb.append(chars.charAt(rand.nextInt(chars.length())))
    }
    
    return sb.toString()
}

