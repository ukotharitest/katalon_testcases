import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.testng.Assert as Assert
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.delay(5)

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://www2.carrierip.net')

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.delay(2)

WebUI.switchToWindowTitle('Sign into your account')

String Expectedpage = 'Dashboard'

WebUI.setText(findTestObject('Page_Sign into your account/input_LoginFormusername'), GlobalVariable.UserName)

WebUI.setText(findTestObject('Page_Sign into your account/input_LoginFormpassword'), GlobalVariable.Password)

WebUI.click(findTestObject('Page_Sign into your account/button_Login'))

WebUI.delay(5)

WebUI.click(findTestObject('Page_Admin/a_Tenant'))

WebUI.click(findTestObject('Page_Tenant/h6_Search'))

WebUI.delay(3)

WebUI.setText(findTestObject('Page_Tenant/input_TenantMasterSearchtm_nam'), 'Utkarsh')

WebUI.click(findTestObject('Page_Tenant/button_Search'))

WebUI.delay(5)

WebUI.click(findTestObject('Page_Tenant/i_fa fa-sign-in'))

WebUI.delay(3)

WebUI.switchToWindowTitle('Tenant Dashboard')

WebUI.delay(5)

WebUI.click(findTestObject('Page_Tenant Dashboard/i_icon_plus'))

WebUI.click(findTestObject('Page_Tenant Dashboard/a_Users'))

WebUI.click(findTestObject('Page_User/h6_Search'))

WebUI.setText(findTestObject('Page_User/input_ExtensionMasterSearchem_'), 'Utkarsh')

WebUI.click(findTestObject('Page_User/button_Search'))

WebUI.delay(5)

WebUI.click(findTestObject('Page_User/i_fa fa-sign-in'))

WebUI.delay(3)

WebUI.setText(findTestObject('Page_User Dashboard/input_live-search-box'), 'Audio Library')

WebUI.click(findTestObject('Page_User Dashboard/a_Audio Library'))

String chars = 'abcdefghijklmnopqrstuvwxyz123456789'

//Add AudioFile 1
WebUI.click(findTestObject('Object Repository/Page_Audio Library/a_Add New'))

String AudioLibraryName1 = ''

AudioLibraryName1 = randomString('AUTOMATION' + chars, 5)

GlobalVariable.AudioFile1 = AudioLibraryName1

WebUI.setText(findTestObject('Object Repository/Page_Create/input_AudioManagementaudiofile'), AudioLibraryName1)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Create/select_Select TypeLOCALREMOTE'), 'LOCAL', true)

WebUI.delay(3)

WebUI.uploadFile(findTestObject('Object Repository/Page_Create/div_file-caption form-control'), 'E:/UploadFile/SampleAudio.mp3')

WebUI.click(findTestObject('Object Repository/Page_Create/button_Save'))

//Add AudioFile 2
WebUI.click(findTestObject('Object Repository/Page_Audio Library/a_Add New'))

String AudioLibraryName2 = ''

AudioLibraryName2 = randomString('AUTOMATION' + chars, 5)

GlobalVariable.AudioFile2 = AudioLibraryName2

WebUI.setText(findTestObject('Object Repository/Page_Create/input_AudioManagementaudiofile'), AudioLibraryName2)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Create/select_Select TypeLOCALREMOTE'), 'LOCAL', true)

WebUI.delay(3)

WebUI.uploadFile(findTestObject('Object Repository/Page_Create/div_file-caption form-control'), 'E:/UploadFile/SampleAudio1.mp3')

WebUI.click(findTestObject('Object Repository/Page_Create/button_Save'))

//Search Audio
WebUI.click(findTestObject('Object Repository/Page_Audio Library/h6_Search'))

WebUI.setText(findTestObject('Object Repository/Page_Audio Library/input_AudioManagementSearchaud'), AudioLibraryName1)

WebUI.click(findTestObject('Object Repository/Page_Audio Library/button_Search'))

WebUI.delay(3)

String audioName = WebUI.getText(findTestObject('Object Repository/Page_Audio Library/td_TestAudio.mp3'))

println(audioName)

println(AudioLibraryName1)

if (audioName.contains(AudioLibraryName1)) {
    Assert.assertTrue(true)

    println('Audio File Verified')
} else {
    Assert.assertTrue(false)

    println('Audio File Not Verified')
}

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_Audio Library/a_Settings'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_Caller ID/a_Music On Hold'))

WebUI.delay(2)

//WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Music On Hold/select_DefaultNoneTestAudio.mp'), 'TestAudio.mp3',  true)
WebUI.sendKeys(findTestObject('Object Repository/Page_Music On Hold/select_DefaultNoneTestAudio.mp'), AudioLibraryName1)

WebUI.click(findTestObject('Object Repository/Page_Music On Hold/button_Apply'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Page_Music On Hold/input_live-search-box'), 'Audio Library')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_Music On Hold/a_Audio Library'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_Audio Library/h6_Search'))

WebUI.setText(findTestObject('Object Repository/Page_Audio Library/input_AudioManagementSearchaud'), AudioLibraryName1)

WebUI.click(findTestObject('Object Repository/Page_Audio Library/button_Search'))

WebUI.delay(3)

String enabledisablevalue = WebUI.getAttribute(findTestObject('Object Repository/Page_Audio Library/i_fa fa-trash'), 'class')

if (enabledisablevalue.contains('disabled')) {
    //btn btn-danger btn-sm disabled
    //btn btn-danger btn-sm
    println('Audio Is Assigned so Not able to Delete')
} else {
    println('Audio Is not Assigned')
}

static String randomString(String chars, int length) {
    Random rand = new Random()

    StringBuilder sb = new StringBuilder()

    for (int i = 0; i < length; i++) {
        sb.append(chars.charAt(rand.nextInt(chars.length())))
    }
    
    return sb.toString()
}

