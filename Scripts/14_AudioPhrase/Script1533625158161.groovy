import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(3)

not_run: WebUI.switchToWindowTitle('')

WebUI.setText(findTestObject('Page_User Dashboard/input_live-search-box'), 'Audio Phrase')

WebUI.click(findTestObject('Page_User Dashboard/a_Audio Phrase'))

WebUI.click(findTestObject('Object Repository/Page_Audio Phrase/a_Add New'))

String AudioPhraseName = ''

String chars = 'abcdefghijklmnopqrstuvwxyz123456789'

AudioPhraseName = randomString('AUTOMATION' + chars, 5)

GlobalVariable.AudioPhrase = AudioPhraseName

WebUI.setText(findTestObject('Page_Create Audio Phrase/input_AudioPhraseap_name'), AudioPhraseName)

WebUI.click(findTestObject('Page_Create Audio Phrase/button_Save'))

WebUI.click(findTestObject('Object Repository/Page_Audio Phrase/h6_Search'))

WebUI.setText(findTestObject('Object Repository/Page_Audio Phrase/input_AudioPhraseSearchap_name'), AudioPhraseName)

WebUI.click(findTestObject('Object Repository/Page_Audio Phrase/button_Search'))

WebUI.click(findTestObject('Object Repository/Page_Audio Phrase/i_fa fa-cogs'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_Configuration/a_btn btn-primary btn-sm fa fa'))

WebUI.delay(2)

//WebUI.selectOptionByValue(findTestObject('Page_Configuration/select_Nokia.mp31wNqT.mp3test1'), '923', true)
WebUI.sendKeys(findTestObject('Page_Configuration/select_Nokia.mp31wNqT.mp3test1'), GlobalVariable.AudioFile1)

WebUI.setText(findTestObject('Page_Configuration/input_apd_time_out1'), '10')

not_run: WebUI.click(findTestObject('Page_Configuration/button_Apply'))

WebUI.click(findTestObject('Page_Configuration/a_btn btn-primary btn-sm fa fa'))

WebUI.delay(2)

//WebUI.selectOptionByValue(findTestObject('Page_Configuration/select_Nokia.mp31wNqT.mp3test1'), '925', true)
WebUI.sendKeys(findTestObject('Page_Configuration/select_Nokia.mp31wNqT.mp3test2'), GlobalVariable.AudioFile2)

WebUI.setText(findTestObject('Page_Configuration/input_apd_time_out2'), '10')

not_run: WebUI.click(findTestObject('Page_Configuration/button_Apply'))

WebUI.click(findTestObject('Page_Configuration/button_Update'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_Audio Library/a_Settings'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_Caller ID/a_Music On Hold'))

WebUI.delay(2)

//WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Music On Hold/select_DefaultNoneTestAudio.mp'), 'TestAudio.mp3',  true)
WebUI.sendKeys(findTestObject('Page_Music On Hold/select_DefaultNoneTestAudio.mp'), AudioPhraseName)

WebUI.delay(2)

WebUI.click(findTestObject('Page_Music On Hold/button_Apply'))

WebUI.delay(2)

WebUI.setText(findTestObject('Page_User Dashboard/input_live-search-box'), 'Audio Phrase')

WebUI.click(findTestObject('Page_User Dashboard/a_Audio Phrase'))

WebUI.click(findTestObject('Object Repository/Page_Audio Phrase/h6_Search'))

WebUI.setText(findTestObject('Object Repository/Page_Audio Phrase/input_AudioPhraseSearchap_name'), AudioPhraseName)

WebUI.click(findTestObject('Object Repository/Page_Audio Phrase/button_Search'))

//WebUI.click(findTestObject('Object Repository/Page_Audio Phrase/a_btn btn-danger btn-sm'))
String enabledisablevalue = WebUI.getAttribute(findTestObject('Object Repository/Page_Audio Phrase/a_btn btn-danger btn-sm'), 
    'class')

if (enabledisablevalue.contains('disabled')) {
    //btn btn-danger btn-sm disabled
    //btn btn-danger btn-sm
    println('AudioPhrase Is Assigned so Not able to Delete')
} else {
    println('AudioPhrase Is not Assigned')
}

static String randomString(String chars, int length) {
    Random rand = new Random()

    StringBuilder sb = new StringBuilder()

    for (int i = 0; i < length; i++) {
        sb.append(chars.charAt(rand.nextInt(chars.length())))
    }
    
    return sb.toString()
}

