import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.testng.Assert as Assert

WebUI.click(findTestObject('Page_Admin/span_Services'))

WebUI.click(findTestObject('Page_Admin/a_Service List'))

WebUI.click(findTestObject('Page_Service Master/b_25'))

WebUI.click(findTestObject('Page_Service Master/a_Service Package Template'))

WebUI.click(findTestObject('Page_Service Package Template/a_Add New'))

String chars = 'abcdefghijklmnopqrstuvwxyz0123456789'

String ServicePackageName = ''

ServicePackageName = randomString('AUTOMATION' + chars, 5)

GlobalVariable.ServicePackage = ServicePackageName

WebUI.setText(findTestObject('Object Repository/Page_Create Service Package Templat/input_ServicePackagesp_name'), ServicePackageName)

WebUI.click(findTestObject('Object Repository/Page_Create Service Package Templat/em_Tap to Assign Services'))

WebUI.click(findTestObject('Object Repository/Page_Create Service Package Templat/input_selection_5'))

WebUI.doubleClick(findTestObject('Object Repository/Page_Create Service Package Templat/input_16'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Page_Create Service Package Templat/input_selection_6'))

WebUI.doubleClick(findTestObject('Object Repository/Page_Create Service Package Templat/input_1'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(4)

WebUI.setText(findTestObject('Object Repository/Page_Create Service Package Templat/input_1'), '2')

//Select User

WebUI.click(findTestObject('Page_Create Service Package Templat/checkUserCheckbox'))

WebUI.doubleClick(findTestObject('Page_Create Service Package Templat/input_user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Page_Create Service Package Templat/input_user'), '2')

//Select DiskQuota

WebUI.click(findTestObject('Page_Create Service Package Templat/checkDiskQuotaCheckbox'))

WebUI.doubleClick(findTestObject('Page_Create Service Package Templat/input_diskquota'))

WebUI.delay(2)

WebUI.setText(findTestObject('Page_Create Service Package Templat/input_diskquota'), '0.05')

//Select MOH

WebUI.click(findTestObject('Page_Create Service Package Templat/checkMOH'))

WebUI.delay(2)

//Save Services

WebUI.click(findTestObject('Object Repository/Page_Create Service Package Templat/button_Save'))

WebUI.click(findTestObject('Page_Service Package Template/h6_Search'))

WebUI.setText(findTestObject('Page_Service Package Template/input_ServicepackageSearchsp_n'), ServicePackageName)

WebUI.click(findTestObject('Page_Service Package Template/button_Search'))

WebUI.click(findTestObject('Page_Service Package Template/span_'))

static String randomString(String chars, int length) {
    Random rand = new Random()

    StringBuilder sb = new StringBuilder()

    for (int i = 0; i < length; i++) {
        sb.append(chars.charAt(rand.nextInt(chars.length())))
    }
    
    return sb.toString()
}

