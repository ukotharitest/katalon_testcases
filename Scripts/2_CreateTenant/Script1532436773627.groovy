import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.testng.Assert as Assert
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Page_Admin/a_Tenant'))

String Expectedpage = 'Dashboard'

String chars = 'abcdefghijklmnopqrstuvwxyz123456789'

String ExpectedTenantName = ''

ExpectedTenantName = randomString('AUTOMATION' + chars, 5)

GlobalVariable.TenantName = ExpectedTenantName

WebUI.click(findTestObject('Page_Tenant/a_Add New'))

WebUI.setText(findTestObject('Page_Create Tenant/input_TenantMastertm_name'), ExpectedTenantName)

WebUI.setText(findTestObject('Page_Create Tenant/input_TenantMastertm_email'), ExpectedTenantName + '@mailinator.com')

WebUI.setText(findTestObject('Page_Create Tenant/input_TenantMastertm_username'), ExpectedTenantName)

WebUI.setEncryptedText(findTestObject('Page_Create Tenant/input_TenantMastertm_password'), 'aeHFOx8jV/A=')

WebUI.delay(5)

WebUI.selectOptionByIndex(findTestObject('Page_Create Tenant/span_Select a Timezone'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.setText(findTestObject('Page_Create Tenant/input_TenantMastertm_web_domai'), ExpectedTenantName + 'web')

WebUI.setText(findTestObject('Page_Create Tenant/input_TenantMastertm_sip_domai'), ExpectedTenantName + 'sip')

WebUI.setText(findTestObject('Page_Create Tenant/input_TenantMastertm_provision'), ExpectedTenantName + 'provisioning')

WebUI.selectOptionByValue(findTestObject('Page_Create Tenant/select_SelectUsing Service Pac'), 'SP_ID', true)

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Page_Create Tenant/select_SelectecosmobteamTest P_2'), 5, FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByLabel(findTestObject('Page_Create Tenant/select_SelectecosmobteamTest P_2'), GlobalVariable.ServicePackage, 
    true)

//WebUI.setText(findTestObject('Page_Create Tenant/select_SelectecosmobteamTest P_2'), GlobalVariable.ServicePackage)
WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('Page_Create Tenant/select_Selectstagingstaging_cl'), '7', true)

WebUI.delay(5)

WebUI.click(findTestObject('Page_Create Tenant/button_Save'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Page_Configure DIDs/input_q'), GlobalVariable.DIDNumber)

//WebUI.selectOptionByValue(findTestObject('Page_Configure DIDs/select_did1'), '', true)
WebUI.click(findTestObject('Object Repository/Page_Configure DIDs/select_did1'))

WebUI.click(findTestObject('Object Repository/Page_Configure DIDs/i_fa fa-arrow-right'))

WebUI.click(findTestObject('Object Repository/Page_Configure DIDs/button_Update'))

WebUI.scrollToElement(findTestObject('Page_Create Tenant/a_Cancel'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Create Tenant/a_Cancel'))

not_run: WebUI.click(findTestObject('Page_Admin/a_Tenant'))

WebUI.click(findTestObject('Page_Tenant/h6_Search'))

WebUI.setText(findTestObject('Page_Tenant/input_TenantMasterSearchtm_nam'), ExpectedTenantName)

WebUI.click(findTestObject('Page_Tenant/button_Search'))

not_run: WebUI.click(findTestObject('Page_Tenant/a_btn btn-warning btn-sm'))

WebUI.click(findTestObject('Page_Tenant/i_fa fa-sign-in'))

WebUI.delay(3)

WebUI.switchToWindowTitle('Tenant Dashboard')

WebUI.delay(3)

String ActualTenantName = WebUI.getText(findTestObject('Object Repository/Page_Admin/h4_63'))

if (ExpectedTenantName.equals(ActualTenantName)) {
    Assert.assertTrue(true)
} else {
    Assert.assertTrue(false)
}

WebUI.delay(3)

WebUI.click(findTestObject('Page_Tenant/b_Mr. Admin'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Tenant/a_Logout'))

WebUI.delay(3)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Tenant')

WebUI.delay(3)

static String randomString(String chars, int length) {
    Random rand = new Random()

    StringBuilder sb = new StringBuilder()

    for (int i = 0; i < length; i++) {
        sb.append(chars.charAt(rand.nextInt(chars.length())))
    }
    
    return sb.toString()
}

