import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.testng.Assert as Assert
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String chars = '123456789'

String DIDNumber = ''

DIDNumber = randomString('12345' + chars, 5)

not_run: WebUI.click(findTestObject('Page_Admin/span_DID Inventory'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Admin/a_DID Individual'))

WebUI.click(findTestObject('Page_DID Individual/a_Add New'))

WebUI.selectOptionByValue(findTestObject('Page_Create DID Inventory/select_RegularToll-Free'), 'tollfree', true)

WebUI.selectOptionByValue(findTestObject('Page_Create DID Inventory/select_NumberRange'), 'number', true)

WebUI.setText(findTestObject('Page_Create DID Inventory/input_Didmasterdid_number'), 'fsfsdfsdfs')

WebUI.click(findTestObject('Page_Create DID Inventory/button_Save'))

String actual = WebUI.getText(findTestObject('Page_Create DID Inventory/div_Number must be an integer.'))

String validation = 'Number must be an integer.'

if (actual.equals(validation)) {
    Assert.assertTrue(true)
} else {
    Assert.assertFalse(true)
}

WebUI.setText(findTestObject('Page_Create DID Inventory/input_Didmasterdid_number'), DIDNumber)

WebUI.setText(findTestObject('Page_Create DID Inventory/textarea_Didmasterdid_descript'), '\\test')

WebUI.click(findTestObject('Page_Create DID Inventory/button_Save'))

WebUI.click(findTestObject('Object Repository/Page_DID Individual/h6_Search'))

WebUI.setText(findTestObject('Object Repository/Page_DID Individual/input_DidMasterSearchdid_numbe'), DIDNumber)

WebUI.click(findTestObject('Object Repository/Page_DID Individual/button_Search'))

String gridDIDNumber = WebUI.getText(findTestObject('Object Repository/Page_DID Individual/td_1234567890'))

if (gridDIDNumber.equals(DIDNumber)) {
    Assert.assertTrue(true)
} else {
    Assert.assertFalse(true)
}

static String randomString(String chars, int length) {
    Random rand = new Random()

    StringBuilder sb = new StringBuilder()

    for (int i = 0; i < length; i++) {
        sb.append(chars.charAt(rand.nextInt(chars.length())))
    }
    
    return sb.toString()
}

