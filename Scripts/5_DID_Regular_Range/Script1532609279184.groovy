import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.testng.Assert as Assert
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.click(findTestObject('Page_Admin/span_DID Inventory'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Admin/a_DID Individual'))

WebUI.click(findTestObject('Page_DID Individual/a_Add New'))

WebUI.selectOptionByValue(findTestObject('Page_Create DID Inventory/select_RegularToll-Free'), 'regular', true)

WebUI.selectOptionByValue(findTestObject('Page_Create DID Inventory/select_NumberRange'), 'range', true)

int i

int FromNumberGreter

int FromNumber

Random rand = new Random()

FromNumber = rand.nextInt(99999999)

FromNumberGreter = (FromNumber + 2)

String FromNumberGreter1 = FromNumberGreter.toString()

String FromNumber1 = FromNumber.toString()

WebUI.setText(findTestObject('Object Repository/Page_Create DID Inventory/input_Didmasterfrom'), FromNumberGreter1)

WebUI.setText(findTestObject('Object Repository/Page_Create DID Inventory/input_Didmasterto'), FromNumber1)

WebUI.click(findTestObject('Page_Create DID Inventory/button_Save'))

String ToValidationExpected = 'To must be greater than or equal to "From".'

String ToActual = WebUI.getText(findTestObject('Object Repository/Page_Create DID Inventory/div_To must be greater than or'))

if (ToActual.equals(ToValidationExpected)) {
    Assert.assertTrue(true)
} else {
    Assert.assertTrue(false)
}

WebUI.setText(findTestObject('Object Repository/Page_Create DID Inventory/input_Didmasterfrom'), 'dsfsdfsdf')

WebUI.click(findTestObject('Page_Create DID Inventory/button_Save'))

WebUI.delay(2)

String actual = WebUI.getText(findTestObject('Object Repository/Page_Create DID Inventory/div_from_must_be_integer'))

String validation = 'From must be an integer.'

if (actual.equals(validation)) {
    Assert.assertTrue(true)
} else {
    Assert.assertFalse(true)
}

WebUI.setText(findTestObject('Object Repository/Page_Create DID Inventory/input_Didmasterfrom'), FromNumber1)

WebUI.setText(findTestObject('Object Repository/Page_Create DID Inventory/input_Didmasterto'), FromNumberGreter1)

WebUI.setText(findTestObject('Page_Create DID Inventory/textarea_Didmasterdid_descript'), '\\test')

WebUI.click(findTestObject('Page_Create DID Inventory/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Page_DID Individual/h6_Search'))

WebUI.setText(findTestObject('Object Repository/Page_DID Individual/input_DidMasterSearchdid_numbe'), FromNumber1)

WebUI.click(findTestObject('Object Repository/Page_DID Individual/button_Search'))

String gridDIDNumber = WebUI.getText(findTestObject('Object Repository/Page_DID Individual/td_1234567890'))

println(gridDIDNumber)

if (gridDIDNumber.equals(FromNumberGreter1)) {
    Assert.assertTrue(true)
} else {
    Assert.assertFalse(true)
}

