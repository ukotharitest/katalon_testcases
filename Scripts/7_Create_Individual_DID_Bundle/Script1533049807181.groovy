import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.testng.Assert as Assert
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(3)

not_run: WebUI.click(findTestObject('Page_Admin/span_DID Inventory'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Admin/a_DID Bundle'))

WebUI.click(findTestObject('Page_DID Bundle/a_Add New'))

String chars = 'abcdefghijklmnopqrstuvwxyz123456789'

String ExpectedTenantName = ''

BundleName = randomString('AUTOMATION' + chars, 5)

WebUI.setText(findTestObject('Page_Create DID Bundle/input_DidBundledb_name'), BundleName)

WebUI.selectOptionByValue(findTestObject('Page_Create DID Bundle/select_RangeIndividualImport'), 'individual', true)

WebUI.setText(findTestObject('Page_Create DID Bundle/input_q'), GlobalVariable.DIDFrom)

//WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Create DID Bundle/select_26749912112674991212267'), '134', 
// true)
WebUI.doubleClick(findTestObject('Page_Create DID Bundle/option_9001'))

WebUI.click(findTestObject('Page_Create DID Bundle/i_glyphicon glyphicon-chevron-'))

WebUI.setText(findTestObject('Page_Create DID Bundle/input_q'), GlobalVariable.DIDTo)

//WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Create DID Bundle/select_26749912112674991212267'), '135', 
// true)
WebUI.doubleClick(findTestObject('Page_Create DID Bundle/option_9002'))

WebUI.click(findTestObject('Page_Create DID Bundle/i_glyphicon glyphicon-chevron-'))

WebUI.click(findTestObject('Page_Create DID Bundle/button_Save'))

WebUI.click(findTestObject('Page_DID Bundle/h6_Search'))

WebUI.setText(findTestObject('Page_DID Bundle/input_DidBundleSearchdb_name'), BundleName)

WebUI.click(findTestObject('Page_DID Bundle/button_Search'))

String bundleName = WebUI.getText(findTestObject('Page_DID Bundle/td_Test1'))

if (bundleName.equals(bundleName)) {
    Assert.assertTrue(true)
} else {
    Assert.assertTrue(false)
}

static String randomString(String chars, int length) {
    Random rand = new Random()

    StringBuilder sb = new StringBuilder()

    for (int i = 0; i < length; i++) {
        sb.append(chars.charAt(rand.nextInt(chars.length())))
    }
    
    return sb.toString()
}

