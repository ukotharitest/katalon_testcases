import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.testng.Assert as Assert
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www2.carrierip.net/auth/auth/login')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Page_Sign into your account/input_LoginFormusername'), 'admin@netcarrier.com')

WebUI.setEncryptedText(findTestObject('Page_Sign into your account/input_LoginFormpassword'), 'aQeHcsDYD3OJb74znlUspQ==')

WebUI.click(findTestObject('Page_Sign into your account/button_Login'))

WebUI.setText(findTestObject('Object Repository/Page_Admin/input_live-search-box'), 'Global Configuration')

WebUI.click(findTestObject('Object Repository/Page_Admin/a_Global Configuration'))

WebUI.click(findTestObject('Object Repository/Page_Global Configuration/h6_Search'))

WebUI.setText(findTestObject('Object Repository/Page_Global Configuration/input_GlobalConfSearchgwc_key'), 'session')

WebUI.click(findTestObject('Object Repository/Page_Global Configuration/button_Search'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_Global Configuration/i_fa fa-pencil'))

WebUI.setText(findTestObject('Object Repository/Page_Update Global Configuration/input_GlobalConfiggwc_value'), '1')

WebUI.click(findTestObject('Object Repository/Page_Update Global Configuration/button_Apply'))

WebUI.delay(122)

String Actualloginpagename = WebUI.getText(findTestObject('Object Repository/Page_Sign into your account/h5_Sign into your account'))

String ExptectedloginpageName = 'Sign into your account'

if (ExptectedloginpageName.equals(Actualloginpagename)) {
    Assert.assertTrue(true)
} else {
    Assert.assertTrue(false)
}

WebUI.setText(findTestObject('Page_Sign into your account/input_LoginFormusername'), 'admin@netcarrier.com')

WebUI.setEncryptedText(findTestObject('Page_Sign into your account/input_LoginFormpassword'), 'aQeHcsDYD3OJb74znlUspQ==')

WebUI.click(findTestObject('Page_Sign into your account/button_Login'))

WebUI.setText(findTestObject('Object Repository/Page_Admin/input_live-search-box'), 'Global Configuration')

WebUI.click(findTestObject('Object Repository/Page_Admin/a_Global Configuration'))

WebUI.click(findTestObject('Object Repository/Page_Global Configuration/h6_Search'))

WebUI.setText(findTestObject('Object Repository/Page_Global Configuration/input_GlobalConfSearchgwc_key'), 'session')

WebUI.click(findTestObject('Object Repository/Page_Global Configuration/button_Search'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_Global Configuration/i_fa fa-pencil'))

WebUI.setText(findTestObject('Object Repository/Page_Update Global Configuration/input_GlobalConfiggwc_value'), '15')

WebUI.click(findTestObject('Object Repository/Page_Update Global Configuration/button_Apply'))

WebUI.click(findTestObject('Page_Service Package Template/span_'))

WebUI.delay(8)

WebUI.click(findTestObject('Page_Tenant/b_Mr. Admin'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Tenant/a_Logout'))

