import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www2.carrierip.net/auth/auth/login')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Page_Sign into your account/input_LoginFormusername'), 'admin@netcarrier.com')

WebUI.setEncryptedText(findTestObject('Page_Sign into your account/input_LoginFormpassword'), 'aQeHcsDYD3OJb74znlUspQ==')

WebUI.click(findTestObject('Page_Sign into your account/button_Login'))

WebUI.setText(findTestObject('Page_Admin/input_live-search-box'), 'Global Configuration')

WebUI.click(findTestObject('Page_Admin/a_Global Configuration'))

WebUI.click(findTestObject('Page_Global Configuration/h6_Search'))

WebUI.setText(findTestObject('Page_Global Configuration/input_GlobalConfSearchgwc_key'), 'Timezone')

WebUI.click(findTestObject('Page_Global Configuration/button_Search'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Global Configuration/i_fa fa-pencil'))

WebUI.selectOptionByIndex(findTestObject('Page_Global Configuration/select_timezone'), 5, FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(5)

not_run: WebUI.click(findTestObject('Page_Update Global Configuration/b_4 (1)'))

not_run: WebUI.delay(3)

not_run: WebUI.setText(findTestObject('Page_Update Global Configuration/input_select2-search__field_1'), 'America/Chicago')

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Page_Update Global Configuration/input_select2-search__field_1'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.sendKeys(findTestObject('Page_Global Configuration/select_TimeZone_2'), Keys.chord(Keys.ENTER))

not_run: WebUI.setText(findTestObject('Page_Global Configuration/select_timezone'), 'America/Chicago')

WebUI.delay(3)

WebUI.click(findTestObject('Page_Update Global Configuration/button_Apply'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Service Package Template/span_'))

WebUI.delay(8)

WebUI.click(findTestObject('Page_Tenant/b_Mr. Admin'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Tenant/a_Logout'))

WebUI.acceptAlert()

WebUI.acceptAlert()

