import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl('http://www2.carrierip.net/auth/auth/login')

WebUI.setText(findTestObject('Page_Sign into your account/input_LoginFormusername'), 'admin@netcarrier.com')

WebUI.setEncryptedText(findTestObject('Page_Sign into your account/input_LoginFormpassword'), '9NLz+4tGZcQ=')

WebUI.click(findTestObject('Page_Admin/a_General (1)'))

WebUI.click(findTestObject('Page_Admin/a_Aggregated Call Data'))

WebUI.navigateToUrl('http://www2.carrierip.net/')

WebUI.click(findTestObject('Page_Admin/a_General (1)'))

WebUI.click(findTestObject('Page_Admin/a_Nagios'))

WebUI.click(findTestObject('Page_Admin/a_Aggregated Call Data'))

WebUI.click(findTestObject('Page_Aggregated Call Data Dashboard/a_Fail2Web'))

WebUI.navigateToUrl('http://www2.carrierip.net/admin/admin')

WebUI.click(findTestObject('Page_Admin/a_Aggregated Call Data'))

WebUI.click(findTestObject('Page_Admin/a_General (1)'))

WebUI.click(findTestObject('Page_Admin/a_Tenant'))

WebUI.click(findTestObject('Object Repository/Page_Tenant/i_icon_plus'))

WebUI.click(findTestObject('Object Repository/Page_Tenant/a_DID Individual'))

