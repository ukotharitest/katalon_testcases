<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>NetCarrier_Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-31T21:07:17</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2d5f6f6c-fa01-4646-a7b4-5abb14b82071</testSuiteGuid>
   <testCaseLink>
      <guid>1829d642-65a1-4a64-96c1-062f9fae3ccd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/6_DID_TollFree_Range</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5c5def3-d6a5-4a76-bdec-818b566fe0cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7_Create_Individual_DID_Bundle</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
