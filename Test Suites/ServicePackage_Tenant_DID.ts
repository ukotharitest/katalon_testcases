<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ServicePackage_Tenant_DID</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-14T11:27:26</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9b7c0b58-3a8d-4daa-902b-03dad9679baa</testSuiteGuid>
   <testCaseLink>
      <guid>776dc7c3-7a12-4d96-be55-33220b82952d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d86b16f-0be4-4d0e-83ef-c7c1fa44f64a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/1_TestCases_ServicePackage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f49ef0ba-bbeb-4ba7-97b6-07f89389344d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/3_DID_Regular_Number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0cd1fdf6-dc00-4d79-bff3-036fc8a916b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/4_DID_TollFree_Number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c0eb174-e996-4151-920d-3246b50b470d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5_DID_Regular_Range</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c3b4bb0-656f-40e0-b82e-702f2a22fb88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/6_DID_TollFree_Range</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3ea6182-8f4a-4065-b76e-1bddf15b74d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/7_Create_Individual_DID_Bundle</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0dde2f80-a124-4035-84bd-960bd6214197</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/2_CreateTenant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>849471f3-ba7f-4fc2-852f-9596fcedf5be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/11_Verify_AssignedDID_ToTenant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab2f2162-5765-432e-b6df-eb64ea43af18</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/12_CreateUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a77136f-d397-440c-9990-a414ec7575c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/13_AudioLibrary</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f38abf8-c8fe-468e-824e-ae238e952714</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/14_AudioPhrase</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a1a1cee-94ac-4f46-8e70-17bf485a7c77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/99_Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
